'''
Author: Mathieu Dugas
'''

# Base imports
import logging as log
import os
import json

# Pypi modules
import redis
import powerdns

# Limited imports
from requests import get
from discord_webhook import DiscordWebhook


# Load config from environment variables
DEBUG = os.getenv("TRACKER_DEBUG", 'True')

DISCORD_WEBHOOK = os.getenv("TRACKER_DISCORD_WEBHOOK")

REDIS_HOST = os.getenv("TRACKER_REDIS_HOST", "localhost")
REDIS_PORT = os.getenv("TRACKER_REDIS_PORT", "6379")

PDNS_API = os.getenv("TRACKER_PDNS_API", "http://localhost:8081")
PDNS_KEY = os.getenv("TRACKER_PDNS_KEY")
PDNS_A_LIST = os.getenv("TRACKER_PDNS_A_LIST")


# Constants and Variables
SEPARATOR = "=============================="
REDIS_KEY = "ip"


# Setup Logger in DEBUG or INFO mode
log.basicConfig(level=log.DEBUG) if DEBUG == 'True' else log.basicConfig(level=log.INFO)

while (True):

    log.info(SEPARATOR)

    # Connect to REDIS
    log.debug("Setting up REDIS connection")
    redis_server = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=0, decode_responses=True)

    # Connect to PowerDNS
    log.debug("Setting up PowerDNS connection")
    pdns_api_client = powerdns.PDNSApiClient(api_endpoint=PDNS_API, api_key=PDNS_KEY)
    pdns_api = powerdns.PDNSEndpoint(pdns_api_client)

    # Grab new IP
    log.debug("Getting current public IP")
    new_ip = get('https://ifconfig.me').content.decode('utf8')
    log.info("Live IP: " + new_ip)

    # Get last IP from redis
    log.debug("Grabbing current saved IP")
    last_ip = redis_server.get('ip') if redis_server.exists(REDIS_KEY) else ''
    log.info("Saved IP: " + last_ip)

    # Grab domain/record list (Array of domain + record)
    # records = ''
    # with open(PDNS_A_LIST) as file:
    #     records = json.load(file)

    # If the IP changed
    if new_ip != last_ip:
        
        # For each A record to update
        # for record in records:
        #     # Update the record in PowerDNS
        #     zone = api.servers[0].get_zone(record['domain'])
        #     zone.create_records([
        #         powerdns.RRSet(record['record'], 'A', [(new_ip, False)])
        #     ])
        #     zone.delete_records([
        #         powerdns.RRSet(record['record'], 'A', [(last_ip, False)])
        #     ])
        #     zone.notify()
        
        # Update IP Address in REDIS
        log.debug("IP Changed; Setting in REDIS")
        redis_server.set(REDIS_KEY, new_ip)
        log.debug("IP in REDIS is now: " + redis_server.get(REDIS_KEY))

        # Send announcement in discord
        DiscordWebhook(url=DISCORD_WEBHOOK, rate_limit_retry=True, content="<@&903514869112926229> Live IP: " + new_ip).execute()