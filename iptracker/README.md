# IP Tracker

 [![pipeline status](https://gitlab.com/kronus.dev/dockerized-application/iptracker/badges/main/pipeline.svg)](https://gitlab.com/kronus.dev/dockerized-application/iptracker/-/commits/main) 

## What is this?
This project aims to track public IP changes for my homelab. It handles the following tasks after it detects an IP change. Change tracking is handled by a Redis db and the DNS updated is using PowerDNS

- Update a list of external DNS A Records in PowerDNS
- Post a notification on a Discord webhook

## Environment Variables
- TRACKER_DEBUG: Enable verbose logging on the console
- TRACKER_DISCORD_WEBHOOK: Webhook URL for posting
- TRACKER_REDIS_HOST: Host for the REDIS backend 
- TRACKER_REDIS_PORT: Port for the REDIS backend
- PDNS_API: PowerDNS API URI (full URI to the API)
- PDNS_KEy: PowerDNS API Key{
- PDNS_A_LIST: Path to a file containing the Domain/Record list

## File example
### PDNS_A_LIST
``` JSON
[
    {
        "domain": "mydomain.tld",
        "record": "www"
    },
    {
        "domain": "mydomain.tld",
        "record": "abcdef"
    },
    {
        "domain": "myseconddomain.tld",
        "record": "www"
    }
]
```