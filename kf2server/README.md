# Killing Floor 2 server

## About

When running this container, the startup script will take the following actions in order:
- Download/update the base game from steam.
- Start the gameserver

If you want to modify the config file, it is located at /data/

## Example
`docker run --name KF2 -d -p 7777:7777/udp -p 27015:27015/udp -p 8080:8080 -p 20560:20560/udp -v /path/on/host/data:/data -e KF2_OPTS="KF-BurningParis?Game=KFGameContent.KFGameInfo_VersusSurvival?maxplayers=12" kr0nus/kf2server:latest`

## Contribution

Anyone is welcome to make contributions to any of my containerization project. Simply fork a project from my [gitlab namespace](https://gitlab.com/kronus.dev/dockerized-application) and submit a PR request stating why you think your . 

---

KF2 Wiki: https://wiki.killingfloor2.com
