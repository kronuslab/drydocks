#!/bin/bash

# Variables Used
#   - KF2_OPTS

# Install/Update the game
/home/steam/steamcmd/steamcmd.sh +login anonymous +force_install_dir /data +app_update 232130 +quit

# Workshop workaround
mkdir -p /data/KFGame/Cache

# Start the server
cd /data  
exec ./Binaries/Win64/KFGameSteamServer.bin.x86_64 ${KF2_OPTS}