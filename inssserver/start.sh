#!/bin/bash

# Variables Used
# BASE_OPTS
# GSLTTOKEN
# SERVER_NAME
# EXTRAOPTS

# Install/Update the game
/home/steam/steamcmd/steamcmd.sh +login anonymous +force_install_dir /data +app_update 581330 validate +quit

# Start the server
cd /data  
exec ./Insurgency/Binaries/Linux/InsurgencyServer-Linux-Shipping ${BASE_OPTS} -hostname=${SERVER_NAME} -log -GameStats -GSLTTOKEN=${GSLTTOKEN} ${EXTRAOPTS}