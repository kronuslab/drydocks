# Insurgency:Sandstorm server

## About

When running this container, the startup script will take the following actions in order:
- Download/update the base game from steam.
- Start the gameserver

If you want to modify the config file, it is located at /data/

## Example
```
docker run --name inssserver -d \
-p 27015:27015 \
-p 27102:27102/udp \
-p 27131:27131/udp \
-p 43769:43769/udp \
-v /path/on/host/data:/data \
-e BASE_OPTS="" \
-e SERVER_NAME="MySuperServer" \
-e GSLTTOKEN="itsasecret" \
kr0nus/inssserver:latest
```

## Contribution

Anyone is welcome to make contributions to any of my containerization project. Simply fork a project from my [gitlab namespace](https://gitlab.com/kronus.dev/dockerized-application) and submit a PR request stating why you think your . 

---

Insurgency:Sandstorm Dedicated Server setup guide: https://sandstorm-support.newworldinteractive.com/hc/en-us/articles/360049211072-Server-Admin-Guide
