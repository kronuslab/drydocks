#!/bin/bash

# Variables Used
#   - STEAM_LOGIN

# Install/Update the game
/home/steam/steamcmd/steamcmd.sh +login ${STEAM_LOGIN} +force_install_dir /data +app_update 211820 +quit

# Install/Update fracking universe
if [[ ! -d /data/mods/FrackinUniverse ]]; then
  git clone https://github.com/sayterdarkwynd/FrackinUniverse.git /data/mods/FrackinUniverse
fi

cd /data/mods/FrackinUniverse
git pull

# Start the server
cd /data/linux
exec ./starbound_server
