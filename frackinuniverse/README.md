# Frackin Universe mod for Starbound

## About

To use this container as designed, the following prerequisites must be met:
- Access to a steam account that has starbound purchased
- The steam account used must not be protected by steamguard

When running this container, the startup script will take the following actions in order:
- Download/update the base game from steam.
- Install/update the mod in /data/mods/FrackinUniverse using git.
- Start the gameserver

If you want to modify the config file, it is located at /data/storage/starbound_server.config

## Example
`docker run --name FrackinUniverse -d -p 21025:21025 -v /path/on/host/data:/data -e STEAM_LOGIN="user password" kr0nus/frackinuniverse:latest`

## Contribution

Anyone is welcome to make contributions to any of my containerization project. Simply fork a project from my [gitlab namespace](https://gitlab.com/kronus.dev/dockerized-application) and submit a PR request stating why you think your . 

---

Original modpack on https://frackinuniverse.fandom.com/wiki/FrackinUniverse_Wiki
