# DryDock project

This is a repository of various containers I designed/built over time. Containers listed here are available from my [DockerHub repo](https://hub.docker.com/search?q=kr0nus&type=image). Each container contains a README file which details how to deploy for regular docker. As a personal reminder of why this repo exists, here's a list of why containers are built here.

- Original work for Kronus's LAB
- Extending an existing container to add a feature
- Working around the currently, as of March 2022, non-working init container chaining of Portainer where depends_on has a condition of service_completed_successfully. [compose spec](https://github.com/compose-spec/compose-spec/blob/master/spec.md#long-syntax-1)

Current TODO list;
- Improve Docker builds to minimize the number of layers a container requires
- Create periodic build pipelines to update the most recent container version on a daily basis.